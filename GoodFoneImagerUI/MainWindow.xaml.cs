﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GoodFoneImagerUI
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		/// <exception cref="COMException">The script function does not exist.</exception>
		/// <exception cref="ObjectDisposedException">The <see cref="T:System.Windows.Controls.WebBrowser" /> instance is no longer valid.</exception>
		public MainWindow()
		{
			InitializeComponent();
			WebBrowser.Navigate("https://vk.com/");
			var r = WebBrowser.InvokeScript("eval", new String[] { "var f = function(){return (2+2).toString();}; f();" });
		}
	}
}
