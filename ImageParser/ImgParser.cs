﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using HtmlAgilityPack;

namespace ImageParser
{
	public class ImgParser
	{
		public int _categoryCount;
		public bool _isPublic;
		public int _sleep = 10000;
		public Random _rndmzr = new Random();

		public ImgParser()
		{
			_categoryCount = LinkTable.Gategories.Count;
		}

		public void PublicImage()
		{
			while (_isPublic)
			{

			}
		}

		public KeyValuePair<string, string> GetRandomCategory()
		{
			int randomCategory = _rndmzr.Next(0, _categoryCount);

			KeyValuePair<string, string> categoryLink = LinkTable.Gategories.ElementAt(randomCategory);

			return categoryLink;
		}

		public string PublicPostInVk(string message, List<string> images)
		{


			return "";
		}

		public Dictionary<string, string> GetRandomImageFromCategory(string url)
		{
			Dictionary<string, string> result = new Dictionary<string, string>();
			   WebClient GodLikeClient = new WebClient();
			HtmlDocument html = new HtmlDocument();

			html.Load(GodLikeClient.OpenRead(url), Encoding.UTF8);
			
			var r = html.DocumentNode.Descendants("div").Where(d => d.Attributes.Contains("class") && d.Attributes["class"].Value.Contains("tabl_td")
				).ToDictionary(el => el.Descendants("a").ToArray().First().Attributes["href"].Value,
				el=>el.Descendants("a").FirstOrDefault(elA => elA.Attributes.Contains("class") && elA.Attributes["class"].Value.Contains("big")).InnerHtml);
			Dictionary<string, string> linksToPage = r.ToDictionary(
				el=>el.Key.IndexOf("https:") == -1 ? $"https://www.goodfon.ru{el.Key}":el.Key, el=>el.Value

				);

			Dictionary<string, string> originalLinks = new Dictionary<string, string>();
			foreach (var item in linksToPage)
			{
				html.Load(GodLikeClient.OpenRead(item.Key), Encoding.UTF8);
				var a= html.DocumentNode.Descendants("b")
					.First(d => d.InnerHtml == "Скачать оригинал:").ParentNode.Descendants("a").First(d => d.Attributes.Contains("href") && d.Attributes["href"].Value.StartsWith("/download/"));
				var linksToOriginal = "https://www.goodfon.ru/"+ a.Attributes.First(attr => attr.Name == "href").Value;
				originalLinks.Add(linksToOriginal, item.Value);
			}

			return linksToPage;
		}
	}
}
