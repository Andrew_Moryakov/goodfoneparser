﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Linq;
using System.Management;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using CefSharp;
using CefSharp.WinForms;
using GoodFonUI.Properties;
using ImageParser;
using Newtonsoft.Json;
using RestSharp;
using Timer = System.Windows.Forms.Timer;

namespace Robot_clicker
{
	public class CustomMenuHandler : CefSharp.IContextMenuHandler
	{
		public void OnBeforeContextMenu(IWebBrowser browserControl, IBrowser browser, IFrame frame, IContextMenuParams parameters, IMenuModel model)
		{
			//model.Clear();
		}

		public bool OnContextMenuCommand(IWebBrowser browserControl, IBrowser browser, IFrame frame, IContextMenuParams parameters, CefMenuCommand commandId, CefEventFlags eventFlags)
		{

			return false;
		}

		public void OnContextMenuDismissed(IWebBrowser browserControl, IBrowser browser, IFrame frame)
		{

		}

		public bool RunContextMenu(IWebBrowser browserControl, IBrowser browser, IFrame frame, IContextMenuParams parameters, IMenuModel model, IRunContextMenuCallback callback)
		{
			return false;
		}
	}
	public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
		}

        private ChromiumWebBrowser chrome;
        private string[] matches;
        private static Timer timer = new Timer();
	    private string[] hashSet = new string[0];
	    private bool isToSend = false;
	    private Dictionary<string, string> _urls = null;


		private void Form1_Load(object sender, EventArgs e)
        {
            CefSettings settings = new CefSettings();
            Cef.Initialize(settings);
            chrome = new ChromiumWebBrowser("https://vk.com/");
            this.pContainer.Controls.Add(chrome);
            chrome.Dock = DockStyle.Fill;
            chrome.AddressChanged += Chrome_AddressChanged;
	        chrome.MenuHandler = new CustomMenuHandler();

			 _urls = new Dictionary<string, string>();

		}

        private void Chrome_AddressChanged(object sender, AddressChangedEventArgs e)
        {
            Invoke(new MethodInvoker(() =>
            {
                txtUrl.Text = e.Address;
            }));
        }

        private void btnGo_Click(object sender, EventArgs e)
        {
	        _isInject = false;

			chrome.Load(txtUrl.Text);
        }

	    private string _key = null;
        private void btnStart_Click(object sender, EventArgs e)
        {
	        _isInject = false;
			btnStart.Enabled = false;
	        btnStop.Enabled = true;

			timer.Interval = 20000;
			timer.Tick += Loop;
			timer.Start();

			//var js = PrepareJS();
			//var task = chrome.EvaluateScriptAsync(js);
			//task.ContinueWith(t =>
			//{
			//    var responce = JsonConvert.SerializeObject(t.Result.Result);
			//    var result = JsonConvert.DeserializeObject<Dictionary<string, object>>(responce);
			//    hashSet = result.Keys.ToArray();
			//});
		}


		private Dictionary<string, string>  LeftJoinOuter(Dictionary<string, string> aList, List<string> bList)
	    {
			Dictionary<string, string> result = new Dictionary<string, string>();

			foreach (var b in bList)
		    {
				foreach (var a in aList)
			    {
				    if (b != a.Key)
				    {
						if(!result.ContainsKey(a.Key))
					    result.Add(a.Key, a.Value);
				    }
			    }
		    }

		    return result;
	    }

	    private string[] GetPostedUrls()
	    {
		    string settingContent = Settings.Default.PostedLinks;
		    string[] postedUrls = settingContent.Split(' ');

		    return postedUrls;

	    }

		private void AddUrlToSetting(string url)
		{
			string settingContent = Settings.Default.PostedLinks += " "+url;
			Settings.Default.Save();

		}

		private Image GetImage()
	    {
		    try
		    {
			    ImgParser imager = new ImgParser();

			    if (_usedLinks >= _urls.Count)
			    {
				    _usedLinks = 0;
				    _urls = imager.GetRandomImageFromCategory(imager.GetAllCategories().First().Key, _page);
				    _urls = LeftJoinOuter(_urls, GetPostedUrls().ToList());
				    _page++;
			    }

			    var client = new RestClient(_urls.ElementAt(_usedLinks).Key);
			    var request = new RestRequest(Method.GET);
			    request.AddHeader("postman-token", "1076be9a-58ba-a767-4026-0357e800f3cd");
			    request.AddHeader("cache-control", "no-cache");
			    byte[] response = client.DownloadData(request);


			    MemoryStream ms = new MemoryStream(response);
			    return Image.FromStream(ms);
		    }
		    catch(Exception ex)
		    {
			    return null;
		    }
	    }

		//   private void CroupImage(Image originImage)
		//   {
		//	Rectangle cropRect = new Rectangle();
		//	Bitmap src = Image.FromFile(fileName) as Bitmap;
		//	Bitmap target = new Bitmap(cropRect.Width, cropRect.Height);

		//	using (Graphics g = Graphics.FromImage(target))
		//	{
		//		g.DrawImage(src, new Rectangle(0, 0, target.Width, target.Height),
		//						 cropRect,
		//						 GraphicsUnit.Pixel);
		//	}

		//}
		public static System.Drawing.Image FixedSize(Image image, int Width, int Height, bool needToFill)
		{
			#region calculations
			int sourceWidth = image.Width;
			int sourceHeight = image.Height;
			int sourceX = 0;
			int sourceY = 0;
			double destX = 0;
			double destY = 0;

			double nScale = 0;
			double nScaleW = 0;
			double nScaleH = 0;

			nScaleW = ((double)Width / (double)sourceWidth);
			nScaleH = ((double)Height / (double)sourceHeight);
			if (!needToFill)
			{
				nScale = Math.Min(nScaleH, nScaleW);
			}
			else
			{
				nScale = Math.Max(nScaleH, nScaleW);
				destY = (Height - sourceHeight * nScale) / 2;
				destX = (Width - sourceWidth * nScale) / 2;
			}

			if (nScale > 1)
				nScale = 1;

			int destWidth = (int)Math.Round(sourceWidth * nScale);
			int destHeight = (int)Math.Round(sourceHeight * nScale);
			#endregion

			System.Drawing.Bitmap bmPhoto = null;
			try
			{
				bmPhoto = new System.Drawing.Bitmap(destWidth + (int)Math.Round(2 * destX), destHeight + (int)Math.Round(2 * destY));
			}
			catch (Exception ex)
			{
				throw new ApplicationException(string.Format("destWidth:{0}, destX:{1}, destHeight:{2}, desxtY:{3}, Width:{4}, Height:{5}",
					destWidth, destX, destHeight, destY, Width, Height), ex);
			}
			using (System.Drawing.Graphics grPhoto = System.Drawing.Graphics.FromImage(bmPhoto))
			{
				grPhoto.InterpolationMode = InterpolationMode.HighQualityBicubic;
				grPhoto.CompositingQuality = CompositingQuality.HighQuality;
				grPhoto.SmoothingMode = SmoothingMode.HighQuality;

				Rectangle to = new System.Drawing.Rectangle((int)Math.Round(destX), (int)Math.Round(destY), destWidth, destHeight);
				Rectangle from = new System.Drawing.Rectangle(sourceX, sourceY, sourceWidth, sourceHeight);
				//Console.WriteLine("From: " + from.ToString());
				//Console.WriteLine("To: " + to.ToString());
				grPhoto.DrawImage(image, to, from, System.Drawing.GraphicsUnit.Pixel);

				return bmPhoto;
			}
		}

		bool _isPosted = true;
		private int _usedLinks = 0;
	    private int _page = 1;
	    private bool _isInject = false;
		Image _img = null;
		private void Loop(object sender, EventArgs e)
	    {
			if(_isPosted == true)
			_img = GetImage();

			if (_img != null)// && _isPosted == true)
			{
				Clipboard.SetText(" ");
				_isPosted = false;
		    //var bitmap = new PictureBox();
			//bitmap.Load(_urls.ElementAt(_usedLinks).Key);
		    string category = _urls.ElementAt(_usedLinks).Value;

		//	if(_isInject == false)
		//	chrome.ShowDevTools();
			_isInject = true;
		var task = chrome.EvaluateScriptAsync(
			@"
				var isPasteImage = false;
				var rTrue = function(){
										return true;
									};
				var rFalse = function(){
										return false;
									};

				var div = document.getElementById('post_field');
				console.log(Wall.showEditPost());
				div.focus();
				if(div.innerHTML == '') 
				{
					console.log('content insert');
					div.innerHTML = '" + _urls.ElementAt(_usedLinks).Key  + @"'
				}

					if(document.querySelectorAll('img.preview').length !== 0 && !isPasteImage)
					{
						console.log('prewiew loaded');
						isPasteImage = true; 
						div.innerHTML = '#wfsmart #wfsmart_" + category + " #" + category + @"';
						document.getElementById('send_post').click();
						console.log('posted');
					}
					else
					{
						console.log('wait loading prewiew');
						isPasteImage = false; 
					}
				

				if(isPasteImage == true)
					rTrue();
				else
					rFalse();
			"
);

		task.ContinueWith(t =>
		{
			if (t?.Result?.Success == true)
			{
				_isPosted = (bool) t.Result.Result;

				if (_isPosted == true)
				{
					AddUrlToSetting(_urls.ElementAt(_usedLinks).Key);
					_usedLinks++;
				}
				else
				{
					chrome.GetBrowser().FocusedFrame.Paste();
				}
			//chrome.GetBrowser().FocusedFrame.Paste();
					//if (((t.Result.Result as Dictionary<dynamic, dynamic>).First().Value as bool?) == false)
					//	timer.Interval += 3000;
				}
		});
			}
		}

        private void btnStop_Click(object sender, EventArgs e)
        {
	        btnStart.Enabled = true;
			btnStop.Enabled = false;
			timer.Stop();
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
	        _isInject = false;

			chrome.Refresh();
        }

        private void btnForward_Click(object sender, EventArgs e)
        {
            if(chrome.CanGoForward)
                chrome.Forward();
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            if(chrome.CanGoBack)
                chrome.Back();
        }

        private void btnDev_Click(object sender, EventArgs e)
        {
            chrome.ShowDevTools();
        }
        
        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            Cef.Shutdown();
        }
		

		private void button2_Click(object sender, EventArgs e)
		{
			
		}

		private void messageToolStripMenuItem_Click(object sender, EventArgs e)
		{
		
		}

		private void toSendToolStripMenuItem_Click(object sender, EventArgs e)
		{
			if (isToSend)
			{
				isToSend = false;
			}
			else
			{
				isToSend = true;
			}
		}
		

		private void Form1_Paint(object sender, PaintEventArgs e)
		{
			if (_isRepaint)
			{
				chrome.Location = new Point(0, flowLayoutPanel1.Size.Height + menuStrip1.Size.Height);
				chrome.Size = new Size(this.Height - flowLayoutPanel1.Size.Height + menuStrip1.Size.Height, this.Width);
			}
		}

	    private bool _isRepaint = false;
		private void repaintToolStripMenuItem_Click(object sender, EventArgs e)
		{
			_isRepaint = !_isRepaint;
		}
	}

	public class BotSetting
	{
		public BotSetting(string keys, string message)
		{
			KeyWords = keys;
			Message = message;
		}

		public string KeyWords { get; set; }
		public string Message { get; set; }
	}
}
