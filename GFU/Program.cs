﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Win32;
using Newtonsoft.Json;
using RestSharp;

namespace Robot_clicker
{
    static class Program
    {
	    public static Form MainForm;
	    private static bool _isTrial = true;
	    private static bool _isActive = true;

	    /// <summary>
	    /// The main entry point for the application.
	    /// </summary>
	    [STAThread]
	    static void Main()
	    {
		    Application.EnableVisualStyles();
		    Application.SetCompatibleTextRenderingDefault(false);
		    MainForm = new Form1();

			Application.Run(MainForm);
		}



		
	}
}
